addi sp sp -32
sd ra 24(sp)
sd s0 16(sp)
addi s0 sp 32

mv a5 a0
mv a4 a1
sw a5 -20(fp)
mv a5 a4
sw a5 -24(fp)

auipc ra 0x0
jalr ra
mv a5 a0
sw a5 -20(fp)

mv a0 a5
ld ra 24(sp)
ld s0 16(sp)
addi sp sp 32
ret 

fun2:
addi sp sp -16
sd s0 8(sp)
addi s0 sp 16

li a5 2

mv a0 a5
ld a0 8(sp)
addi sp sp 16
ret

fun3:
addi sp sp -32
sd s0 24(sp)
addi s0 sp 32

li a5 1
sw a5 -20(s0)

lw a5 -20(s0)

mv a0 a5
ld s0 24(sp)
addi sp sp 32
ret


