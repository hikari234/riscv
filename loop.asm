.text
  lui x1, %hi(label) 	# ar[20]
  add x2, x0, x0 	# i = 0
  loop:
    lb x3, 0(x1)
    sb x3, 0x120(x1)
    addi x1, x1, 1
    addi x4, x0, 4	# time limitation: 4
    blt x2, x4, loop
    
.data
  label: .byte 0x1 0x2 0x3 0x4
    


