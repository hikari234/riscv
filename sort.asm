.text
add  t0, t0, x0		# i = 0
addi t2, x0, 3

loop1:
addi t0 ,t0, 1
addi t1, t1, 0		# j = 0
lui  a0, %hi(arr)

loop2:
addi t1 ,t1, 1		# j++ 
lb  s0, 0(a0)
lb  s1, 1(a0)
blt s1, s0, swap

# no swap
addi a0, a0, 1
blt t1, t2, loop2

blt  t0, t2 , loop1

ret

swap:
sb s1, 0(a0)
sb s0, 1(a0)
addi a0, a0, 1
blt t1, t2, loop2

.data
arr:
    .byte 0x4 0x1 0x2 0x3
